﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactoryView
{
    public static void CreateView(UIView view, Transform parent)
    {
        string path = "Views/" + view.ToString();
        GameObject newView = GameObject.Instantiate(Resources.Load(path)) as GameObject;
        if (newView != null)
        {
            newView.transform.SetParent(parent.transform);
            newView.transform.localPosition = Vector3.zero;
            newView.transform.localScale = Vector3.one;
        }
    }
}
