﻿public enum UIState
{
    Open = 0,
    Close = 1,
    Destroy = 2
}

public enum UIView
{
    None = -1,
    Main = 0,
    LevelSelector = 1,
    HUD = 2,
    GameOver = 3,
    Settings = 4,
    Loading = 5
}

