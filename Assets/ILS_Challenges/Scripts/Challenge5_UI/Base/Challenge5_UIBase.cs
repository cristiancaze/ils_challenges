﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Challenge5_UIBase : MonoBehaviour
{
    protected private Challenge5_ViewModel viewModel;
    
    public static UnityAction<Challenge5_ViewModel> ChangeView;
}
