﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge5_UIManager : MonoBehaviour
{
    private Transform parentView;
    private Challenge5_ViewHandler viewHandler;
    private Challenge5_ViewModel viewModel;
    private Challenge5_ICommand openViewcmd;
    private Challenge5_ICommand closeViewcmd;

    private void Awake()
    {
        parentView = GetComponent<Transform>();
        viewHandler = new Challenge5_ViewHandler();
        viewModel.container = parentView;
        StartUI();

        Challenge5_UIBase.ChangeView = ChangeView;
        
    }

    private void StartUI()
    {
        openViewcmd = viewHandler.OpenCommand();
        closeViewcmd = viewHandler.CloseCommand();
        viewModel.nextView = UIView.HUD;
        openViewcmd.Execute(viewModel);
    }



    private void ChangeView(Challenge5_ViewModel view)
    {
        view.container = parentView;
        closeViewcmd.Execute(view);
        openViewcmd.Execute(view);
    }
}
