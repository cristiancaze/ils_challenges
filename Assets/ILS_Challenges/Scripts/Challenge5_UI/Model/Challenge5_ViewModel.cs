﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Challenge5_ViewModel
{
    public UIView nextView;
    public Transform container;
    public GameObject currentView;
}