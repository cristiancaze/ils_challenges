﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge5_ViewHandler
{
    private Challenge5_ICommand openViewCommand;
    private Challenge5_ICommand closeViewCommand;

    public Challenge5_ICommand OpenCommand() {
        openViewCommand = new Challenge5_OpenCommand();
        return openViewCommand;
    }

    public Challenge5_ICommand CloseCommand() {
        closeViewCommand = new Challenge5_CloseCommand();
        return closeViewCommand;
    }
}
