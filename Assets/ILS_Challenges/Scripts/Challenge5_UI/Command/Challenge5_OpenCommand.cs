﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge5_OpenCommand : Challenge5_ICommand
{
    public void Execute(Challenge5_ViewModel view)
    {
        FactoryView.CreateView(view.nextView, view.container);
    }
}
