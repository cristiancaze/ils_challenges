﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge5_CloseCommand : MonoBehaviour, Challenge5_ICommand
{
    public void Execute(Challenge5_ViewModel view)
    {
        Destroy(view.currentView);
    }
}
