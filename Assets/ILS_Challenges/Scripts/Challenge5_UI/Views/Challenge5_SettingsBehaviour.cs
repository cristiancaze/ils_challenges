﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge5_SettingsBehaviour : Challenge5_UIBase
{
    public void GoToMain() {
        viewModel.currentView = this.gameObject;
        viewModel.nextView = UIView.Main;
        ChangeView(viewModel);
    }
}
