﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge5_MenuBehaviour : Challenge5_UIBase
{
    public void GoToHUD() {
        viewModel.currentView = this.gameObject;
        viewModel.nextView = UIView.HUD;
        ChangeView(viewModel);
    }

    public void GoToSettings() {
        viewModel.currentView = this.gameObject;
        viewModel.nextView = UIView.Settings;
        ChangeView(viewModel);
    }
}
