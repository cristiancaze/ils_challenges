﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Challenge5_HUDBehaviour : Challenge5_UIBase
{
    private Text[] txtCollection;
    private Text lifes;
    private Text timer;

    private void Awake() {
        txtCollection = GetComponentsInChildren<Text>();
        lifes = txtCollection[0];
        timer = txtCollection[1];
    }

    public void GoToMain() {
        viewModel.currentView = this.gameObject;
        viewModel.nextView = UIView.Main;
        ChangeView(viewModel);
        //notificar a uimanager que se abre el main. ui manager notifica
        //  a command y ejecuta el command para cerrar el HUD y abrir el main
    }
    
}
