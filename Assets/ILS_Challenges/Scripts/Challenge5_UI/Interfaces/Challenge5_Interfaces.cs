﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Challenge5_ICommand {
    void Execute(Challenge5_ViewModel view);
}