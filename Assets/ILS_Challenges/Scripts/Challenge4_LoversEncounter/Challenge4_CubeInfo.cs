﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge4_CubeInfo : MonoBehaviour
{
    [Header ("Lover1")]
    [SerializeField]
    private Characters_Actions action1 = Characters_Actions.No_Action;
    [SerializeField]
    private Characters_Actions action2 = Characters_Actions.No_Action;

    [Header("Lover2")]
    [SerializeField]
    private Characters_Actions action3 = Characters_Actions.No_Action;
    [SerializeField]
    private Characters_Actions action4 = Characters_Actions.No_Action;

    public Challenge4_CubeActions GetActionsLover1()
    {
        
        Challenge4_CubeActions actions = new Challenge4_CubeActions();
        actions.action1 = action1;
        actions.action2 = action2;
        return actions;
    }

    public Challenge4_CubeActions GetActionsLover2()
    {
        
        Challenge4_CubeActions actions = new Challenge4_CubeActions();
        actions.action1 = action3;
        actions.action2 = action4;
        return actions;
    }
}
