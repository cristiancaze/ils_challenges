﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Challenge4_CheckerPathManager : MonoBehaviour
{
    public Challenge4_CubeActions CubeChecker(Transform lover)
    {
        RaycastHit hit;
        Challenge4_CubeActions actions = new Challenge4_CubeActions();

        actions.action1 = Characters_Actions.No_Action;
        actions.action2 = Characters_Actions.No_Action;

        Debug.DrawRay(lover.position, -lover.up, Color.blue);
        if (Physics.Raycast(lover.position + lover.forward, -lover.up, out hit))
        {
            if(hit.transform.tag == "Cube")
            {
                Challenge4_ActionBase character = lover.GetComponent<Challenge4_ActionBase>();
                
                if (character.character == Characters.Lover1)
                {
                    actions = hit.transform.GetComponent<Challenge4_CubeInfo>().GetActionsLover1();
                }else
                {
                    actions = hit.transform.GetComponent<Challenge4_CubeInfo>().GetActionsLover2();
                }
            }
        }else
        {
            actions.action1 = Characters_Actions.No_Ground;
            actions.action2 = Characters_Actions.No_Ground;
        }
        return actions;
    }

    public bool LoverChecker(Transform lover)
    {
        RaycastHit hit;
        bool isLookingLover = false;
        Debug.DrawRay(lover.position, lover.forward * 2, Color.red);
        if (Physics.Raycast(lover.position, lover.forward, out hit, 2))
        {
            if(hit.transform.tag == "Lover")
            {
                isLookingLover = true;
            }
        }
        
        return isLookingLover;
    }

    
}
