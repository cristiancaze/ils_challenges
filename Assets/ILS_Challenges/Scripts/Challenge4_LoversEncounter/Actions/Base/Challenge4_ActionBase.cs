﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Challenge4_ActionBase : MonoBehaviour
{
    public static UnityAction<Challenge4_ActionBase, Characters_Actions> CompleteAction;

    public Characters character;

    public virtual void BeginAction(Characters_Actions action)
    {
        
    }

    protected virtual void EndAction(Characters_Actions action)
    {
        if (CompleteAction != null)
        {
            CompleteAction(this, action);
        }
    }


}
