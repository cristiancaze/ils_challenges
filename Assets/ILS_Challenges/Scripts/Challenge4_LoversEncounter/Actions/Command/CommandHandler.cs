﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandHandler
{
    private ICommandAction movementCommand;
    private ICommandAction rotateCommand;

    public ICommandAction MoveCommand() {
        movementCommand = new Challenge4_Movement();
        return movementCommand;
    }

    public ICommandAction RotateCommand() {
        rotateCommand = new Challenge4_Rotation();
        return rotateCommand;
    }
}
