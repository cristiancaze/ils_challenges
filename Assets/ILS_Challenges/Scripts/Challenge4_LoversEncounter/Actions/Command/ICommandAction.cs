﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICommandAction
{
    void Execute(Characters_Actions action);
}
