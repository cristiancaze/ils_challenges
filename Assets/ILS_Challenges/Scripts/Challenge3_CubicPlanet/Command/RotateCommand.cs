using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RotateCommand : Icommand
{
    public static Action CompleteRotation;
    public void Execute(GameObject character)
    {
        character.transform.Rotate(Vector3.right * 90, Space.Self);
        character.transform.Translate(Vector3.forward * 0.5f, Space.Self);
        CompleteRotation();
    }
}