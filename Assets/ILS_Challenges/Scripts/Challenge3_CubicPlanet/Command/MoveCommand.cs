using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCommand : Icommand
{

    private bool canMove = true;
    public bool CanMove
    {
        get {return canMove;}
        set {canMove = value;}
    }

    public void Execute(GameObject character)
    {
        if (canMove) {
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");
            character.transform.Translate(x * 0.1f, 0, y * 0.1f, Space.World);
            character.transform.LookAt(character.transform.position + new Vector3(x,0,y));
        }
    }
}