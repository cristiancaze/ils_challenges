using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler {
    private Icommand movementCommand;
    private Icommand rotationCommand;

    public Icommand MoveCommand(){
        if(Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) {
            movementCommand = new MoveCommand();           
            return movementCommand;
        }
        return null; 
    }

    public Icommand RotateCommand(){
        rotationCommand = new RotateCommand();
        return rotationCommand;
    }
}