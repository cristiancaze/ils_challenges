using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWorld {
    public void RotateAllWorld(Transform worldContainer, Transform  characterContainer, Direction direction) {
        switch (direction)
        {
            case Direction.forward:
                worldContainer.Rotate(Vector3.right * -90, Space.World);
                characterContainer.Rotate(Vector3.right * -90, Space.World);
            break;

            case Direction.back:
                worldContainer.Rotate(Vector3.right * 90, Space.World);
                characterContainer.Rotate(Vector3.right * 90, Space.World);
            break;

            case Direction.right:
                worldContainer.Rotate(Vector3.forward * 90, Space.World);
                characterContainer.Rotate(Vector3.forward * 90, Space.World);
            break;

            case Direction.left:
                worldContainer.Rotate(Vector3.forward * -90, Space.World);
                characterContainer.Rotate(Vector3.forward * -90, Space.World);
            break;
            
            default:
            break;
        }
        
    }
}