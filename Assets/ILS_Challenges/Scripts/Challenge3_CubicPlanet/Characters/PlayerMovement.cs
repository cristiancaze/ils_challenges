﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstefaniaMovement : MonoBehaviour
{

    RayChecker rayChecker;
    private bool canMove = true;
    public bool CanMove
    {
        get {return canMove;}
        set {canMove = value;}
    }

    private Vector3 currentPosition;
    public Vector3 CurrentPosition
    {
        get {return currentPosition;}
        set {currentPosition = value;}
    }

    void Awake()
    {
        rayChecker = GetComponent<RayChecker>();
    }
    
    private void FixedUpdate()
    {
        if (canMove) {
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");
            transform.Translate(x, 0, y, Space.Self);
            //transform.LookAt(transform.position + new Vector3(x,0,y));


        }
    }

}

