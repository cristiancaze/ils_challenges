﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstefaniaRotation : MonoBehaviour
{
    
    private EstefaniaMovement estefaniaMovement;
    


    void Start()
    {
        estefaniaMovement = GetComponent<EstefaniaMovement>();
    }

    public void Rotate() {
        if (transform.position != estefaniaMovement.CurrentPosition) {
            transform.Rotate(Vector3.right * 90, Space.Self);
            estefaniaMovement.CurrentPosition = transform.position;
            estefaniaMovement.CanMove = true;
        }
    }


}
