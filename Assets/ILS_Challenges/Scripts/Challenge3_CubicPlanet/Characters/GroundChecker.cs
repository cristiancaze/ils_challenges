﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayChecker
{
    RaycastHit hit;
    private Vector3 maxBound;
    private Vector3 minBound;



    public bool CheckGround(Transform character) {

        bool isGround = false;
        Ray floorRay = new Ray(character.position, -character.up);
        if (Physics.Raycast(floorRay, out hit))
        {
            Debug.DrawRay(character.position, -character.up, Color.red);
            maxBound = hit.collider.bounds.max;
            minBound = hit.collider.bounds.min;
            isGround = true;
        }
        return isGround;
    }

    public Direction RotateDirection(Transform character) {
            if (character.position.z >= maxBound.z)
            {
                return Direction.forward;
            }
            if (character.position.z <= minBound.z)
            {
                return Direction.back;
            }
            if (character.position.x >= maxBound.x)
            {
                return Direction.right;
            }
            if (character.position.x <= minBound.x)
            {
                return Direction.left;
            }
            return default;
    }
}
