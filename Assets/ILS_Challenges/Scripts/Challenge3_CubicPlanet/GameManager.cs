﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private InputHandler inputHandler;
    private MoveCommand moveBehavior;
    private RotateWorld rotateWorld;
    private RayChecker rayChecker;
    [SerializeField] private GameObject character;
    [SerializeField] private GameObject characterContainer, worldContainer;
    
    
    
    private void Start()
    {
        RotateCommand.CompleteRotation = OnCompleteRotation;
        rotateWorld = new RotateWorld();
        moveBehavior = new MoveCommand();
        inputHandler = new InputHandler();
        rayChecker = new RayChecker();
    }

    // Update is called once per frame
    private void Update()
    {
        if (inputHandler.MoveCommand() != null) {
            if (rayChecker.CheckGround(character.transform)) {
                Icommand moveCmd = inputHandler.MoveCommand();
                moveCmd.Execute(character);
            } else {
                Icommand rotateCmd = inputHandler.RotateCommand();
                rotateCmd.Execute(character);  
            }
        }
        
    }

    private void OnCompleteRotation() {
        Direction nextDirection = rayChecker.RotateDirection(character.transform);
        rotateWorld.RotateAllWorld(worldContainer.transform, characterContainer.transform, nextDirection);
    }
}
