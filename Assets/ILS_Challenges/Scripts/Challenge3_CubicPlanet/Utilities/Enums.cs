public enum Direction {
    forward = 0,
    right = 1,
    back = 2,
    left = 3,
}