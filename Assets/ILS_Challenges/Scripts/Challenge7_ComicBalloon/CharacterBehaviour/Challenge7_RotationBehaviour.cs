﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge7_RotationBehaviour : MonoBehaviour
{
    public void RotateForward()
    {
        transform.Rotate(Vector3.right * 90, Space.Self);
    }
}
