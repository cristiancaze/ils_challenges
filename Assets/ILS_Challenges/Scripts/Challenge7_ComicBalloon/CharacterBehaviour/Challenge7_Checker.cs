﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge7_Checker : MonoBehaviour
{
    private RaycastHit hit;

    public bool GroundChecker()
    {
        
        bool isGround = false;

        if (Physics.Raycast(transform.position + transform.forward * 0.1f, -transform.up, out hit))
        {  
            Debug.DrawRay(transform.position, -transform.up, Color.red);
            isGround = true;
        }
       
        return isGround;
    }

    public bool ObstacleChecker()
    {
        bool isObstacleInFront = false;
        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            Debug.DrawRay(transform.position + Vector3.up, transform.forward, Color.red);
            isObstacleInFront = true;
        }
        return isObstacleInFront;
    }
}
