﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge7_MovementBehaviour : MonoBehaviour
{
    private Challenge7_RotationBehaviour rotation;
    private Challenge7_Checker checker;

    private Vector3 lastPosition;

    private void Awake()
    {
        checker = GetComponent<Challenge7_Checker>();
        rotation = GetComponent<Challenge7_RotationBehaviour>();
        lastPosition = transform.position;
    }

    private void Update()
    {
        if (checker.GroundChecker())
        {
            lastPosition = transform.position;

            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            transform.Translate(x * 0.1f, 0, y * 0.1f, Space.World);
            transform.LookAt(transform.position + new Vector3(x, 0, y));
        }
        else
        {
            transform.position = lastPosition;
        }
        
    }

    
}
