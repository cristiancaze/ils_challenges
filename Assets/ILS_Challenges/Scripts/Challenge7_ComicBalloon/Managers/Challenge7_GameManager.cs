﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge7_GameManager : MonoBehaviour
{

    public Challenge7_PoolMessageManager poolMessages;

    // Start is called before the first frame update
    private void Start()
    {
        XMLManager.LoadXML("Messages");
        XMLManager.LoadMessages();

        poolMessages.Initialize();
        Challenge7_Sensor.IsNear = OnNearNPC;
       
    }

    private void OnNearNPC(bool isNear, Transform parent)
    {
        int idText = parent.GetComponent<Challenge7_NPCBehaviour>().idText;
        poolMessages.MessageControl(isNear, XMLManager.Messages[idText].message, parent);
    }
}
