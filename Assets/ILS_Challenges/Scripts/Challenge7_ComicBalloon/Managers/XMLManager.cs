﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class XMLManager
{
	private static XmlDocument xmlDocument;
	private static TextAsset textXML;

	public static List<Challenge7_Message> Messages;

	public static void LoadXML (string xmlName)
	{
		textXML = Resources.Load ("XML/" + xmlName) as TextAsset;
		xmlDocument = new XmlDocument ();
		xmlDocument.LoadXml (textXML.ToString ());
    }

	public static void LoadMessages ()
	{
        string numberMessageValue = xmlDocument.DocumentElement.SelectSingleNode("/Messages/NumberMessages").InnerText;
        int numberMessages = int.Parse(numberMessageValue);

        Messages = new List<Challenge7_Message>();
        Messages.Clear(); 

        for (int i = 1; i <= numberMessages; i++)
        {
            Challenge7_Message message = new Challenge7_Message();
            message.message = xmlDocument.DocumentElement.SelectSingleNode("/Messages/Message"+i).InnerText;
            Messages.Add(message);
        }
	}

	
}

