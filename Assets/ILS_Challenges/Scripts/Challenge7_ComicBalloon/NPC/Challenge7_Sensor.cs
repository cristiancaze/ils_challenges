﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Challenge7_Sensor : MonoBehaviour
{
    public static UnityAction<bool, Transform> IsNear;

    private void OnTriggerEnter(Collider character)
    {
        if(character.tag == "Player")
        {
            if(IsNear != null)
            {
                IsNear(true, transform.parent);
            }
        }
    }

    private void OnTriggerExit(Collider character)
    {
        if (character.tag == "Player")
        {
            if (IsNear != null)
            {
                IsNear(false, transform.parent);
            }
        }
    }
}
