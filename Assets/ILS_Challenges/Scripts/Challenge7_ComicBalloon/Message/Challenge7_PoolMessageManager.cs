﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge7_PoolMessageManager : MonoBehaviour
{
    private Challenge7_MessageBehavior[] messageObjects;

    public void Initialize()
    {
        messageObjects = GetComponentsInChildren<Challenge7_MessageBehavior>();
        DefaultValues();
    }

    private void DefaultValues()
    {
        for(int i = 0; i < messageObjects.Length; i++)
        {
            messageObjects[i].Initialize();
        }
    }

    public void MessageControl(bool state, string message, Transform parent)
    {
        for (int i = 0; i < messageObjects.Length; i++)
        {
            if (state)
            {
                if (!messageObjects[i].IsActive)
                {
                    messageObjects[i].ShowMessage(message, parent);
                    break;
                }
            }
            else
            {
                if (messageObjects[i].IsActive && messageObjects[i].transform.parent == parent)
                {
                    messageObjects[i].HideMessage();
                }
            }
        }
    }

}
