﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Challenge7_MessageBehavior : MonoBehaviour
{
    private TextMeshProUGUI message;

    private bool isActive;
    public bool IsActive
    {
        get
        {
            return isActive;
        }
    }

    private Transform defaultParent;

    public void Initialize()
    {
        message = GetComponentInChildren<TextMeshProUGUI>();
        defaultParent = transform.parent;
        HideMessage();
    }

    public void ShowMessage(string messageValue, Transform parent)
    {
        gameObject.SetActive(true);
        transform.SetParent(parent);
        transform.localPosition = new Vector3(0, 4, 0);
        transform.LookAt(parent.transform.forward);
        transform.localScale = Vector3.one;
        message.text = messageValue;
        isActive = true;
    }

    public void HideMessage()
    {
        isActive = false;
        message.text = "";
        transform.SetParent(defaultParent);
        transform.localPosition = Vector3.zero;
        transform.rotation = Quaternion.identity;
        transform.localScale = Vector3.one;
        gameObject.SetActive(false);
    }
}
